﻿using System;
namespace DLVO.models
{
    /// <summary>
    /// Setting up a new Formula:
    ///     public property for formulaparameters 
    ///     //empty line
    ///     Constructor:
    ///         every formulaparameter is entered as a constructorparameter in on a new line
    ///         assign every constructorparameter to the properties
    ///     //empty line
    ///     Calculate method
    /// </summary>
    interface IFormula
    {
        double Calculate(double h);
        string NameInLegend();
    }

    class VanDerWaals : IFormula
    {
        public double Lambda { get; set; }
        public double Hammacker { get; set; }
        public double ParticleRadius { get; set; }

        public VanDerWaals
            (
                double lambda, 
                double hammacker, 
                double particleRadius
            )
        {
            this.Lambda = lambda;
            this.Hammacker = hammacker;
            this.ParticleRadius = particleRadius;
        }

        public double Calculate(double h)
        {
            return 
                -(Hammacker * ParticleRadius) 
                / 
                (6 * h * Math.Pow((1 + (14 * h / Lambda)), 2));
        }

        public string NameInLegend()
        {
            return "Van Der Waals";
        }
    }
    class DoubleLayer : IFormula
    {
        public double ParticleRadius { get; set; }
        public double ElementaryChargeConstant { get; set; }
        public double DielectricConstantFluid { get; set; }
        public double Z { get; set; }
        public double SurfacePotentialParticle { get; set; }
        public double SurfacePotentialWall { get; set; }
        public double BoltzmannConstant { get; set; }
        public double Temperature { get; set; }
        public double AvogadroConstant { get; set; }
        public double IonicStrength { get; set; }
        public double Vacuumpermittivity { get; set; }

        public DoubleLayer
            (
                double particleRadius,
                double elementaryChargeConstant,
                double dielectricConstantFluid,
                double z,
                double surfacePotentialParticle,
                double surfacePotentialWall,
                double boltzmannConstant,
                double temperature,
                double AvogadroConstant,
                double IonicStrength,
                double Vacuumpermittivity
            )
        {
            this.ParticleRadius = particleRadius;
            this.ElementaryChargeConstant = elementaryChargeConstant;
            this.DielectricConstantFluid = dielectricConstantFluid;
            this.Z = z;
            this.SurfacePotentialParticle = surfacePotentialParticle;
            this.SurfacePotentialWall = surfacePotentialWall;
            this.BoltzmannConstant = boltzmannConstant;
            this.Temperature = temperature;
            this.AvogadroConstant = AvogadroConstant;
            this.IonicStrength = IonicStrength;
            this.Vacuumpermittivity = Vacuumpermittivity;
        }

        public double Calculate(double h)
        {
            double k = InverseDebyeLength();
            return
                (
                    (64 * Math.PI * ParticleRadius * DielectricConstantFluid * Vacuumpermittivity) *
                    Math.Pow(((BoltzmannConstant * Temperature) / (Z * ElementaryChargeConstant)), 2) *
                    Math.Tanh((Z * ElementaryChargeConstant * SurfacePotentialParticle) / (4 * BoltzmannConstant * Temperature)) *
                    Math.Tanh((Z * ElementaryChargeConstant * SurfacePotentialWall) / (4 * BoltzmannConstant * Temperature)) *
                    Math.Exp(-InverseDebyeLength() * h)
                );

        }
        private double InverseDebyeLength()
        {
            return 
                1/(Math.Sqrt(
                    (DielectricConstantFluid * Vacuumpermittivity * BoltzmannConstant * Temperature) /
                    (2000 * Math.Pow(ElementaryChargeConstant, 2) * AvogadroConstant * IonicStrength)
                    
                ));
        }

        public string NameInLegend()
        {
            return "Double Layer";
        }
    }
    class BornRepulsion : IFormula
    {
        public double ParticleRadius { get; set; }
        public double BornCollisionRange { get; set; }
        public double hammacker { get; set; }

        public BornRepulsion
            (
                double particleRadius, 
                double bornCollisionRange, 
                double hammacker
            )
        {
            this.ParticleRadius = particleRadius;
            this.BornCollisionRange = bornCollisionRange;
            this.hammacker = hammacker;
        }

        public double Calculate(double h)
        {
            return
                (
                    ((hammacker * Math.Pow(BornCollisionRange , 6)) / 7560) *
                    (
                        ((8 * ParticleRadius + h) / Math.Pow(2 * ParticleRadius + h, 7)) +
                        (6 * ParticleRadius - h) / Math.Pow(h,7)
                    )
                );
        }

        public string NameInLegend()
        {
            return "Born Repulsion";
        }
    }
    class RepulsiveHydration : IFormula
    {
        public double ParticleRadius { get; set; }
        public double ParticleForceConstant { get; set; } //C 1
        public double WallForceConstant { get; set; } //c 2
        public double ParticleDecayLength { get; set; } // Lambda 1
        public double WallDecayLength { get; set; } //Lambda 2

        public RepulsiveHydration
            (
                double particleRadius, 
                double particleForceConstant, //c 1
                double wallForceConstant, //c 2
                double particleDecayLength, //Lambda1
                double wallDecayLength //Lambda2
            )
        {
            this.ParticleRadius = particleRadius;
            this.ParticleForceConstant = particleForceConstant;
            this.ParticleDecayLength = particleDecayLength;
            this.WallDecayLength = wallDecayLength;
            this.WallForceConstant = wallForceConstant;
        }

        public double Calculate(double h)
        {
            return
            (
                ParticleRadius *
                (
                    (ParticleForceConstant * ParticleDecayLength * Math.Exp(-h/ParticleDecayLength)) 
                    +
                    (WallForceConstant * WallDecayLength) * Math.Exp(-h/WallDecayLength)
                )
            );
        }

        public string NameInLegend()
        {
            return "Repulsive Hydration";
        }
    }
    class DLVOFormula : IFormula
    {
        public VanDerWaals VanderWaals { get; set; }
        public DoubleLayer DoubleLayer { get; set; }

        public DLVOFormula
            (
                VanDerWaals vanderWaals, 
                DoubleLayer doubleLayer
            )
        {
            this.VanderWaals = vanderWaals;
            this.DoubleLayer = doubleLayer;
        }

        public double Calculate(double h)
        {
            return this.VanderWaals.Calculate(h) + this.DoubleLayer.Calculate(h);
        }

        public string NameInLegend()
        {
            return "Classic DLVO";
        }
    }
    class ExtDLVOFormula : IFormula
    {
        public DLVOFormula DLVOEnergyformula { get; set; }
        public BornRepulsion BornRepulsion { get; set; }
        public RepulsiveHydration RepulsiveHydration { get; set; }

        public ExtDLVOFormula
            (
                VanDerWaals vanderWaals, 
                DoubleLayer doubleLayer, 
                BornRepulsion bornRepulsion, 
                RepulsiveHydration repulsiveHydration
            )
        {
            this.DLVOEnergyformula = new DLVOFormula(vanderWaals, doubleLayer);
            this.BornRepulsion = bornRepulsion;
            this.RepulsiveHydration = repulsiveHydration;
        }

        public double Calculate(double h)
        {
            return DLVOEnergyformula.Calculate(h) + this.BornRepulsion.Calculate(h) + this.RepulsiveHydration.Calculate(h);
        }

        public string NameInLegend()
        {
            return "Extended DLVO";
        }
    }
    class FormulaName : IFormula //just an unused formula to explain adaptations
    {
        public double VariableName { get; set; } //Properties
        public int A { get; set; } = 8;
        public int B { get; set; } = 5;

        public FormulaName(double variableName, int a, int b) //Constructor method (parameters of the method)
        {
            VariableName = variableName; //assigning the parameters to the properties
            A = a;
            B = b;
        }

        public double Calculate(double h) //calculate method, part of the IFormula interface, this method is called by the CalcLine method
        {
            return B * Math.Exp(-A * h) + VariableName;
        }

        public string NameInLegend()//
        {
            return "Formula Name";
        }
    }
}

