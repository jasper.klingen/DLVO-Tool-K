﻿using System.Windows;

using DLVO.ViewModel;

namespace DLVO
{
    public partial class DLVOView : Window
    {
        public DLVOView()
        {
            InitializeComponent();
            this.DataContext = new DlvoViewModel(); 
        }
    }
}
