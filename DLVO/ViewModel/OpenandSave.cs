﻿using DLVO.Command;
using DLVO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DLVO
{
    public class OpenandSave
    {
        public static DlvoModel Openfile()
        {
            Microsoft.Win32.OpenFileDialog openXmlDialog = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "XML file (*.xml)|*.xml"
            };
            Nullable<bool> result = openXmlDialog.ShowDialog();
            if (result == true)
            {
                DlvoModel model = ReadandWrite.ReadDlvoModelXmlFile(openXmlDialog.FileName);
                if (model != null) return model;
            }
            MessageBox.Show("Unable to open file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            return null;
        }
        public static void Savefile(DlvoModel model)
        {

            Microsoft.Win32.SaveFileDialog saveXmlDialog = new Microsoft.Win32.SaveFileDialog
            {
                Filter = "XML file (*.xml)|*.xml"
            };
            Nullable<bool> result = saveXmlDialog.ShowDialog();
            if (result == true)
                ReadandWrite.WriteDlvoModelXmlFile(model, saveXmlDialog.FileName);
            else
                MessageBox.Show("Unable to save file", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
