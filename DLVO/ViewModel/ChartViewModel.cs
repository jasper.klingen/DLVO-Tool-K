﻿using DLVO.Model;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using LiveCharts.Defaults;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Globalization;

namespace DLVO.ViewModel
{
    public class ChartViewModel
    {
        public SeriesCollection Table
        {
            get { return table; }
            set { table = value; }
        }

        public ZoomingOptions ZoomMode
        {
            get { return zoommode; }
            set { zoommode = value; }
        }
        public ChartViewModel()
        {
            zoommode = ZoomingOptions.None;
            Table = new SeriesCollection();

            var mapper = Mappers.Xy<MeasureModel>()
                .X(mappermodel => mappermodel.H)
                .Y(mappermodel => mappermodel.Y);
            Charting.For<MeasureModel>(mapper);

        }
        private ZoomingOptions zoommode;
        private SeriesCollection table;
    }
}